import {
    Button,
    Checkbox,
    FormControlLabel,
    FormLabel,
    TextField,
    Typography,
    Select,
    MenuItem,
} from "@mui/material";
import './addBook.styles.css';
import { Box } from "@mui/system";
import axios from "axios";
import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Navbar from "../../components/layouts/navbar/Navbar";

const AddBook = () => {
    const navigate = useNavigate();
    const [data, setData] = useState([]);
    const [image, setImage] = useState('');
    const [inputs, setInputs] = useState({
        name: "",
        ISBN: "",
        description: "",
        price: "",
        stock: "",
        author: "",
        publishedYear: "",

        //image: "",
        category: 0,
    });

    useEffect(() => {
        axios.get('http://localhost:3001/api/v1/category').then((res) => {
            console.log(res.data[0].name);
            setData(res.data);
        }).catch((err) => console.log(err));
    }, []);

    //const [checked, setChecked] = useState(false);
    const handleChange = (e) => {
        setInputs((prevState) => ({
            ...prevState,
            [e.target.name]: e.target.value,
        }));
        console.log(e.target.name, "Value", e.target.value);
    };

    const handleImageChange = (e) => {
        console.log(e.target.files);
        setImage(e.target.files[0]);
    };

    const sendRequest = async () => {
        const formData = new FormData();
        formData.append('name', inputs.name);
        formData.append('description', inputs.description);
        formData.append('price', inputs.price);
        formData.append('author', inputs.author);
        formData.append('categoryId', inputs.category);
        formData.append('image', image);
        formData.append('ISBN', inputs.ISBN);
        formData.append('publishedYear', inputs.publishedYear);
        formData.append('stock', inputs.stock);

        await axios
            .post("http://localhost:3001/api/v1/books", formData)
            .then((res) => res.data);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(inputs);
        sendRequest().then(() => navigate("/books"));
    };

    return (
        <div>
            <Navbar darkTheme={true}></Navbar>
            <form onSubmit={handleSubmit}>
                <Box
                    display="flex"
                    flexDirection="column"
                    justifyContent={"center"}
                    maxWidth={700}
                    alignContent={"center"}
                    alignSelf="center"
                    marginLeft={"auto"}
                    marginRight="auto"
                    marginTop={10}
                >
                    <FormLabel>ISBN</FormLabel>
                    <TextField
                        value={inputs.ISBN}
                        onChange={handleChange}
                        margin="normal"
                        fullWidth
                        variant="outlined"
                        name="ISBN"
                    />
                    <FormLabel>Name</FormLabel>
                    <TextField
                        value={inputs.name}
                        onChange={handleChange}
                        margin="normal"
                        fullWidth
                        variant="outlined"
                        name="name"
                    />
                    <FormLabel>Author</FormLabel>
                    <TextField
                        value={inputs.author}
                        onChange={handleChange}
                        margin="normal"
                        fullWidth
                        variant="outlined"
                        name="author"
                    />
                    <FormLabel>Description</FormLabel>
                    <TextField
                        value={inputs.description}
                        onChange={handleChange}
                        margin="normal"
                        fullWidth
                        variant="outlined"
                        name="description"
                    />
                    <FormLabel>Published Year</FormLabel>
                    <TextField
                        value={inputs.publishedYear}
                        onChange={handleChange}
                        type="number"
                        margin="normal"
                        fullWidth
                        variant="outlined"
                        name="publishedYear"
                    />
                    <FormLabel>Price</FormLabel>
                    <TextField
                        value={inputs.price}
                        onChange={handleChange}
                        type="number"
                        margin="normal"
                        fullWidth
                        variant="outlined"
                        name="price"
                    />
                    <FormLabel>Stock</FormLabel>
                    <TextField
                        value={inputs.stock}
                        onChange={handleChange}
                        type="number"
                        margin="normal"
                        fullWidth
                        variant="outlined"
                        name="stock"
                    />
                    <FormLabel>Category</FormLabel>
                    <Select
                        value={inputs.category}
                        name="category"
                        fullWidth
                        variant="outlined"
                        onChange={handleChange}
                    >
                        {
                            data.map((item) => <MenuItem key={item.id} value={item.id}>{item.name}</MenuItem>)
                        }
                    </Select>
                    <input type="file" onChange={handleImageChange} />

                    <Button variant="contained" type="submit">
                        Add Book
                    </Button>
                </Box>
            </form>
        </div>
    );
};

export default AddBook;