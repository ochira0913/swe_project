import React from "react";
import './productListingCard.styles.css';
import { Link } from 'react-router-dom';

//const url = "https://images.unsplash.com/photo-1543002588-bfa74002ed7e?q=80&w=1887&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D";

const ProductListingCard = ({ bookData }) => {
    return (
        <div className="product-listing-card">
            <div className="product-listing-img-container">
                <img src={'http://localhost:3001/images/' + bookData.imgPath} alt="product-listing" className="product-listing-image" />
            </div>
            <div className="product-listing-details-container">
                <h3>{bookData.name}</h3>
                <p className="author-name">{bookData.author}</p>
                <p className='pricing'>₮{bookData.price}</p>
            </div>
            <div className="card-btn-container">
                <Link to={`/book-details/${bookData.id}`} className="product-listing-button">Сагслах</Link>
            </div>
        </div>
    )
}

export default ProductListingCard;