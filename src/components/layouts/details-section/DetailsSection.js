import React, { useState, useEffect, useContext } from 'react';
import './detailsSection.styles.css';
import { useParams, useNavigate } from 'react-router-dom';
import { UserContext, CartContext } from '../../../App';
import axios from 'axios';

//const url = "https://images.unsplash.com/photo-1543002588-bfa74002ed7e?q=80&w=1887&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D";

const DetailsSection = () => {
    const { id } = useParams();
    const [bookData, setBookData] = useState({});

    const user = useContext(UserContext);
    const { cartItems, setCartItems } = useContext(CartContext);

    const navigate = useNavigate();

    const fetchInfo = () => {
        return axios.get("http://localhost:3001/api/v1/books").then((res) => {
            let newData = (res.data).filter((book) => book.id === parseInt(id));
            setBookData(newData[0]);
        }).catch((err) => console.log(err));
    };

    useEffect(() => {
        fetchInfo();
    }, [id])

    const handleAddToCart = () => {
        if (user) {
            //add to cart
            setCartItems([...cartItems, bookData]);
            alert(`The book ${bookData.book_name} is added to the cart`);
        } else {
            navigate('/login');
            alert("Please Login or Sign up first..");
        }
    }

    return (
        <section className="detail-section-container">
            <div className='container'>
                <div className="flex-container">
                    <div className='book-img-container'>
                        <img src={'http://localhost:3001/images/' + bookData.imgPath} alt="book" />
                    </div>

                    <div className='book-detail-container'>
                        <h2>{bookData.name}</h2>
                        <p className="text-primary">{bookData.author}</p>
                        <p className="book-description">{bookData.book_description}</p>
                        <p><b>Description</b>: {bookData.description}</p>
                        <p><b>Stock</b> : {bookData.stock}</p>

                        <h3>₮{bookData.price}</h3>

                        <a onClick={handleAddToCart} className="button-primary">Add To Cart</a>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default DetailsSection;