import React, { useEffect, useState } from 'react';
import './productListing.styles.css';
import ProductListingCard from '../../cards/product-listing-card/ProductListingCard';
import axios from 'axios';

const ProductListing = () => {
    const [data, setData] = useState([]);
    const fetchInfo = () => {
        return axios.get("http://localhost:3001/api/v1/books").then((res) => setData(res.data)).catch((err) => console.log(err));
    };

    useEffect(() => {
        fetchInfo();
    }, []);

    console.log(data);

    return (
        <div className="product-listing-container">
            <div className="container">
                <h2>Санал <span className="text-primary">Номын</span> Жагсаалт</h2>

                <div className="listing-container">
                    {data.slice(0, 4).map((book) => (
                        <ProductListingCard key={book.id} bookData={book} />
                    ))}
                </div>

            </div>
        </div>
    )
}

export default ProductListing;