import React, { useEffect, useState } from 'react';
import './productListingAll.styles.css';
import axios from 'axios';

import ProductListingCard from '../../cards/product-listing-card/ProductListingCard';

const ProductListingAll = () => {
    const [data, setData] = useState([]);
    const fetchInfo = () => {
        return axios.get("http://localhost:3001/api/v1/books").then((res) => setData(res.data)).catch((err) => console.log(err));
    };

    useEffect(() => {
        fetchInfo();
    }, []);

    console.log(data);

    return (
        <section className="product-listing-all-container">
            <div className="container">

                <div className="grid-container">
                    {data.map((book) => {
                        return (
                            <div key={book.id} className="grid-item">
                                <ProductListingCard bookData={book} />
                            </div>
                        )
                    })}

                </div>
            </div>
        </section>
    )
}

export default ProductListingAll;