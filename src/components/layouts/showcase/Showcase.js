import React from "react";
import './showcase.styles.css'
import Navbar from "../navbar/Navbar";
import SearchInputForm from "../../forms/searchInputForm/SearchInputForm";

const Showcase = () => {
    return (
        <section className="showcase-container">
            <Navbar darkTheme={false} />

            <div className="overlay"></div>
            <div className="showcase-content">
                <h1>Цахим <span className="text-primary">Номын </span>Дэлгүүр</h1>
                <p>Хямд, Найдвартай, Хурдан</p>

                <SearchInputForm darkTheme={true} />
            </div>
        </section>
    )
}

export default Showcase;