import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

const AuthForm = ({ buttonName }) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [username, setUsername] = useState('');
    const navigate = useNavigate();

    const handleSubmit = (event) => {
        event.preventDefault();

        if (buttonName === 'Login') {
            axios.post("http://localhost:3001/api/v1/users/login", {
                email: email,
                password: password
            }).then((res) => {
                if (res.data.success) {
                    localStorage.setItem('userStatus', parseInt(res.data.data.user.status));
                    console.log(localStorage.getItem('userStatus'));
                    navigate('/');
                } else {
                    alert("Invalid email or password!");
                }
            }).catch((err) => console.log(err));

        } else {
            axios.post("http://localhost:3001/api/v1/users/register", {
                firstName: username,
                lastName: username,
                email: email,
                password: password
            }).then((res) => {
                if (res.data.success) {
                    navigate('/');
                } else {
                    alert("Invalid email or password!");
                }
            }).catch((err) => console.log(err));
        }

    }

    return (
        <form onSubmit={handleSubmit}>
            {buttonName === "Sign Up" && (
                <div className='form-group'>
                    <label>Username</label>
                    <input
                        type="text"
                        className='form-input'
                        placeholder='Enter your username'
                        value={username}
                        onChange={(event) => setUsername(event.target.value)}
                        required
                    />
                </div>
            )}

            <div className='form-group'>
                <label>Email</label>
                <input
                    type="email"
                    className='form-input'
                    placeholder='Enter your email'
                    value={email}
                    onChange={(event) => setEmail(event.target.value)}
                    required
                />
            </div>

            <div className='form-group'>
                <label>Password</label>
                <input
                    type="password"
                    className='form-input'
                    placeholder='Enter your password'
                    value={password}
                    onChange={(event) => setPassword(event.target.value)}
                    required
                />
            </div>

            <div className='form-group'>
                <input
                    type="submit"
                    className='button-primary'
                    value={buttonName}
                />
            </div>
        </form>
    )
}

export default AuthForm;

